#include "connect_qml.h"
#include <QDebug>
#include <QThread>
#include <QNetworkDiskCache>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>

Connect_qml::Connect_qml(QObject *parent): QObject(parent)
{
      this->link_download = "";
      this->path_folder = "";
      this->choose = -1;

}

void Connect_qml::Set_link_download(QString link_download)
{
    this->link_download = link_download;
}

void Connect_qml::Set_path(QString path)
{
    this->path_folder = path;
}

QString Connect_qml::Get_link_download()
{
    return this->link_download;
}

QString Connect_qml::Get_path()
{
    return this->path_folder;
}

int Connect_qml::Get_choice()
{
    return this->choose;
}

void Connect_qml::Set_choice(int choice)
{
    this->choose = choice;
}

QString Connect_qml::Speed_download(qint64 bytesReceived, double time)
{
     double speed = bytesReceived * 1000.0 / time;
     QString unit;
     if (speed < 1024) {
         unit = "bytes/sec";
     } else if (speed < 1024*1024) {
         speed /= 1024;
         unit = "kB/s";
     } else {
         speed /= 1024*1024;
         unit = "MB/s";
     }

     return QString::number(speed)  + " " + unit;
}

void Connect_qml::Progress_download_file(qint64 bytesReceived, qint64 bytesTotal, int process, double time)
{
   // double r = (double)bytesReceived;
  //  double s = (double)bytesTotal;
    double s = 0;
    int index = 0;
    double bytesum = 0;

    ByteReceive_process[process] =  bytesReceived;
    ByteTotal_process[process] =  bytesTotal;

    for(index = 0; index < ByteReceive_process.size(); index++)
    {
        s += ByteTotal_process[index];
        bytesum += ByteReceive_process[index];
    }
    qDebug() << "so thread pool la: " << this->pool->maxThreadCount() << "  " << this->pool->activeThreadCount();
    qDebug() << "sum bytes: " << bytesum << ", time: " << time << "total: " << s << " process: " << process;

    emit progress_download_to_qml((bytesum/s), Speed_download(bytesum, time));
}

void Connect_qml::Information_download(int process)
{
    emit infordownload(file->Take_path(), file->Take_filename());
}

void Connect_qml::Fail_down(QString error, QString path, QString filename)
{
    qDebug() << "error down load file " << error << " path: " << path << " name: " << filename ;
    this->file->Removefile(path, filename);
}

void Connect_qml::Hello()
{
    qDebug() << "Hello world";
}

void Connect_qml::Download_simple(QString url, QString path, int process)
{
    QThread *thread = new QThread();
    file = new File(path, url, 0);

    download = new Download_file(path, url, file->Take_filename(), process);
    download->moveToThread(thread);
    connect(thread, SIGNAL(started()), download, SLOT(startNextdownload()));

    //automatically delete thread and task object when work is done:
    connect( thread, SIGNAL(finished()), download, SLOT(deleteLater()));
    connect( thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    connect(download, SIGNAL(progress_download(qint64, qint64, int, double)), this, SLOT(Progress_download_file(qint64, qint64, int, double)));
    connect(download, SIGNAL(finished(int)), this, SLOT(Information_download(int)));
    connect(download, SIGNAL(Error_download_failed(QString, QString, QString)), this, SLOT(Fail_down(QString, QString, QString)));

    connect(download, SIGNAL(finished()), thread, SLOT(quit()));

    thread->start();

    this->pool->reserveThread();

}

void Connect_qml::Download_multi_url()
{
    QStringList Listurl = file->Read_file_json();

    int index = 0;
    //download multi file
    this->pool->setMaxThreadCount(16);
    QString path_new = this->file->Check_path(this->path_save_file_multi + "/listimg");

    for(index = 0; index < Listurl.length(); index++)
    {

       // Download_simple(Listurl[index], this->path_save_file_multi + "/listimg", index);
       // QFuture <void>th = QtConcurrent::run(Download_simple, Listurl[index], this->path_save_file_multi + "/listimg", index);

        QFuture <void>th = QtConcurrent::run(this, &Connect_qml::Download_simple, Listurl[index], path_new, index);
       // th.waitForFinished();
        ByteReceive_process.append(index);
        ByteTotal_process.append(index);
    }
}

//ket noi api download file json va tai ve
void Connect_qml::Download_compound(QString url, QString path)
{

    file = new File(path, url, 1);//tham so 1 tuong ung voi download file json
    download = new Download_file(path, url, file->Take_filename(), 0);//tham so cuoi cung tuong ung
                                                                      //tien trinh
    this->path_save_file_multi = path;

    QThread *thread = new QThread();
    this->pool = new QThreadPool();

    connect(thread, SIGNAL(started()), download, SLOT(startNextdownload()));
    connect( download, SIGNAL(finished()), thread, SLOT(quit()));

    //automatically delete thread and task object when work is done:
    connect( thread, SIGNAL(finished()), download, SLOT(deleteLater()) );
    connect( thread, SIGNAL(finished()), thread, SLOT(deleteLater()) );

    connect( thread, SIGNAL(finished()), this, SLOT(Download_multi_url()) );

    thread->start();
}

//bat yeu cau tin hieu download tu trong qml
void Connect_qml::Signal_download()
{
    switch(Get_choice()){
        case 0:
            ByteReceive_process.clear();
            ByteTotal_process.clear();
            ByteReceive_process.append(0);
            ByteTotal_process.append(0);
            Download_simple(Get_link_download(), Get_path(), 0);
            break;
        case 1:
            ByteReceive_process.clear();
            ByteTotal_process.clear();
            ByteReceive_process.append(0);
            ByteTotal_process.append(0);
            Download_compound(Get_link_download(), Get_path());
            break;
        default:
            break;
    }
}
