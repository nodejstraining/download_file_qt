#ifndef CONNECT_QML_H
#define CONNECT_QML_H

#include <QObject>
#include <QUrl>
#include <QDebug>
#include <QString>
#include <QTimer>
#include <QVector>
#include "File.h"
#include "download_file.h"
#include <QThreadPool>
#include <QRunnable>

class Connect_qml : public QObject
{
    Q_OBJECT

public:
   explicit Connect_qml(QObject *parent = 0);
   QString Get_path();
   QString Get_link_download();
   int Get_choice();
   void Notify_error(int cases);
   void Download_simple(QString url, QString path, int);//download đơn
   void Download_compound(QString url, QString path);//download kép-->kết nối api mới download
   QString Speed_download(qint64, double);
   QVector<double> ByteReceive_process;
   QVector<double> ByteTotal_process;
   QString path_save_file_multi;//download nhieu file
   void Hello();


signals:
    void infordownload(QString namefile, QString path);
    void progress_download_to_qml(double bytes, QString speed);

public slots:
    void Set_path(QString path);
    void Set_link_download(QString link_download);
    void Set_choice(int choice);//download don hay download kep
    void Signal_download();
    void Progress_download_file(qint64, qint64, int, double);
    void Information_download(int);
    void Download_multi_url();
    void Fail_down(QString, QString, QString);

private:
    QString path_folder;
    QString link_download;
    File *file;
    Download_file *download;
    int choose;
    QThreadPool *pool;
};

#endif // CONNECT_QML_H
