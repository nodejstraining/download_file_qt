#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "connect_qml.h"
#include "download_file.h"
#include "File.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QQmlContext* context = engine.rootContext();

    Connect_qml dt;
    context->setContextProperty("connectqml", &dt);

  /*  dt.Download_simple("http://hinhchat.com/wp-content/uploads/2017/01/hinh-anh-dong-lua-xanh-tuoi-agricultural-field-2.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 0);
    dt.ByteReceive_process.append(0);
    dt.Download_simple("http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 1);
    dt.ByteReceive_process.append(0);
    dt.Download_simple("http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 2);
    dt.ByteReceive_process.append(0);
    dt.Download_simple("http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 3);
    dt.ByteReceive_process.append(0);
    dt.Download_simple("http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 4);
    dt.ByteReceive_process.append(0);
    dt.Download_simple("http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 5);
    dt.ByteReceive_process.append(0);
    dt.Download_simple("http://dep.anh9.com/imgs/14111hinh-nen-Google-danh-cho-trang-web.jpg", "E:/Thuc Tap He 2017/Qt basic/data", 6);
    dt.ByteReceive_process.append(0);

    dt.ByteTotal_process.append(0);
    dt.ByteTotal_process.append(0);
    dt.ByteTotal_process.append(0);
    dt.ByteTotal_process.append(0);
    dt.ByteTotal_process.append(0);
    dt.ByteTotal_process.append(0);
    dt.ByteTotal_process.append(0); */

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
